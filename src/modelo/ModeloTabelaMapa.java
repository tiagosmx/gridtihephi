/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Tito
 */
public class ModeloTabelaMapa extends AbstractTableModel {

    private final Object[][] matriz;
    private final int linhas;
    private final int colunas;

    public ModeloTabelaMapa(int linhas, int colunas) {
        this.colunas = colunas;
        this.linhas = linhas;
        matriz = new Object[linhas][colunas];

        for (int x = 0; x < linhas; x++) {
            for (int y = 0; y < colunas; y++) {
                matriz[x][y] = new Vazio(x, y);
                if (x == 0 || y == 0 || x == linhas - 1 || y == colunas - 1) {
                    matriz[x][y] = new Obstaculo(x, y);
                }
            }
        }
    }

    @Override
    public int getRowCount() {
        return this.linhas;
    }

    @Override
    public int getColumnCount() {
        return this.colunas;
    }

    @Override
    public String getValueAt(int linha, int coluna) {
        Object celula = this.matriz[linha][coluna];

        if (celula.getClass() == Vazio.class) {
            return "";
        } else if (celula.getClass() == Obstaculo.class) {
            return "X";
        } else if (celula.getClass() == Robo.class) {
            return "R";
        } else if (celula.getClass() == Chegada.class) {
            return "F";
        } else {
            return "E";
        }
    }

    @Override
    public boolean isCellEditable(int linha, int coluna) {
        if (linha == 0 || coluna == 0 || linha == linhas - 1 || coluna == colunas - 1) {
            return false;
        }
        return true;
    }

    /**
     * Método que é chamado quando altera-se a tabela gráfica
     *
     * @param valor
     * @param linha
     * @param coluna
     */
    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        System.out.println("X linha =" + linha + "Y coluna=" + coluna);
        if (linha == 0 || coluna == 0 || linha == linhas - 1 || coluna == colunas - 1) {
            //não faz nada, pois faz parte da borda.
        } else if ("X".equalsIgnoreCase(valor.toString())) {
            this.matriz[linha][coluna] = new Obstaculo(linha, coluna);
            this.fireTableDataChanged();
        } else if ("".equals(valor.toString().trim())) {
            this.matriz[linha][coluna] = new Vazio(linha, coluna);
            this.fireTableDataChanged();
        } else if ("R".equalsIgnoreCase(valor.toString())) {
            this.matriz[linha][coluna] = new Robo(this, linha, coluna);
            
            this.fireTableDataChanged();
        } else if ("F".equalsIgnoreCase(valor.toString())) {
            Robo r = procurarRobo();
            if (null != r) {
                Chegada c = new Chegada(linha, coluna);
                this.matriz[linha][coluna] = c;
                r.setChegada(c);
                this.fireTableDataChanged();
            } else {
                System.out.println("Robo nao encontrado.");
            }
        } else {

        }
    }

    /**
     * Insere um objeto na célula especifica (não tem filtro, então é
     * independente de qualquer célula, então tomar cuidado.
     *
     * @param objeto
     * @param linha
     * @param coluna
     */
    public void setObjectAt(Object objeto, int linha, int coluna) {
        this.matriz[linha][coluna] = objeto;
        this.fireTableDataChanged();
    }

    /**
     * Retorna o objeto do robô que está na posição informada.
     *
     * @param x
     * @param y
     * @return
     */
    public Robo getRobotAt(int x, int y) {
        if (this.matriz[x][y].getClass() == Robo.class) {
            return (Robo) this.matriz[x][y];
        } else {
            return null;
        }
    }

    public Robo procurarRobo() {
        for (int x = 0; x < linhas; x++) {
            for (int y = 0; y < colunas; y++) {
                Robo robo = getRobotAt(x, y);
                if (robo != null) {
                    return robo;
                }
            }
        }
        return null;
    }
    
    public void limparDadosDasBuscas(){
        for(int x = 0; x<this.linhas; x++){
            for(int y = 0; y<this.colunas; y++){
                try{
                    Celula cel = (Celula)matriz[x][y];
                    cel.limparDadosDeBusca();
                } catch(Error er){
                    System.out.println("Erro ao fazer casting ao limpar dados.");
                    System.out.println(er);
                }
            }
        }
        this.fireTableDataChanged();
    }

    public int getLinhas() {
        return linhas;
    }

    public int getColunas() {
        return colunas;
    }

    public Object getObject(int linha, int coluna) {
        return matriz[linha][coluna];
    }

    public Celula getCelula(int linha, int coluna) {
        return (Celula) matriz[linha][coluna];
    }
}
