/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Tito
 */
public class Celula {

    public int x;
    public int y;
    //G é o custo do movimento
    public int g;
    //H é o custo estimado até o destino (Manhattan)
    public int hEstrela;
    public int hMelhor;
    public Celula paiEstrela;
    public Celula paiMelhor;
    public Celula paiInicioFimRobo;
    public Celula paiInicioFimDestino;
    public boolean isCaminhoAEstrela = false;
    public boolean isCaminhoMelhorPrimeiro = false;
    public boolean isCaminhoInicioFim = false;

    public int getF() {
        return g + hEstrela;
    }

    public int getManhattanAEstrela(int xFim, int yFim) {
        return (Math.abs(x - xFim) + Math.abs(y - yFim)) * 10;
    }

    public int getManhattanMelhorPrimeiro(int xFim, int yFim) {
        return (Math.abs(x - xFim) + Math.abs(y - yFim));
    }

    public void resetarValoresAEstrela() {
        g = 0;
        hEstrela = 0;
        paiEstrela = null;
        isCaminhoAEstrela = false;
    }

    public void resetarValoresMelhorPrimeiro() {
        g = 0;
        hMelhor = 0;
        paiMelhor = null;
        isCaminhoMelhorPrimeiro = false;
    }
    
    public void resetarValoresInicioFim(){
        paiInicioFimRobo = null;
        paiInicioFimDestino = null;
        isCaminhoInicioFim = false;
    }
    
    public void limparDadosDeBusca(){
        this.g = 0;
        this.hEstrela = 0;
        this.hMelhor = 0;
        this.isCaminhoAEstrela = false;
        this.isCaminhoInicioFim = false;
        this.isCaminhoMelhorPrimeiro = false;
        this.paiEstrela = null;
        this.paiInicioFimRobo = null;
        this.paiInicioFimDestino = null;
        this.paiMelhor = null;
    }
    
}
