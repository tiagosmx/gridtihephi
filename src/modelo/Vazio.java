/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Tito
 */
public class Vazio extends Celula{
    
    public Vazio(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    //Aqui armazenamos os parâmetros para a geração dos caminhos
    /*Parâmetros para o A* */
    int xPaiA;
    int yPaiA;
    int distanciaA;
    
    /*Fim dos parâmatros A*/
    
    /*Parâmetros para o Inicio-Fim */
    int xPaiInicioFim;
    int yPaiInicioFim;
    /* Fim dos parâmetros para o Inicio-Fim */

    /*Parâmetros para o Melhor-Primeiro */
    int xPaiMelhorPrimeiro;
    int yPaiMelhorPrimeiro;
    int distanciaMelhorPrimeiro;
    /*Fim dos parâmetros para o Melhor-Primeiro */
}
