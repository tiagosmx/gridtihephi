/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Tito
 */
public class Robo extends Celula {

    private final ModeloTabelaMapa mapa;

    public Chegada chegada;

    public Robo(ModeloTabelaMapa mapa, int x, int y) {
        this.mapa = mapa;
        this.x = x;
        this.y = y;
        this.g = 0;
    }

    public void setChegada(Chegada c) {
        this.chegada = c;
        hEstrela = super.getManhattanAEstrela(c.x, c.y);
        hMelhor = super.getManhattanMelhorPrimeiro(c.x, c.y);
    }

    public String mostrarLocalizacao() {
        for (int x = 0; x < mapa.getLinhas(); x++) {
            for (int y = 0; y < mapa.getColunas(); y++) {
                if (mapa.getObject(x, y).equals(this)) {
                    return "Objeto móvel está em linha:" + x + " coluna: " + y;
                }
            }
        }
        return "Objeto móvel não encontrado.";
    }

    public int retornaPosX() {
        int posX = 0;
        for (int x = 0; x < mapa.getLinhas(); x++) {
            for (int y = 0; y < mapa.getColunas(); y++) {
                if (mapa.getObject(x, y).equals(this)) {
                    posX = x;
                }
            }
        }
        return posX;
    }

    public int retornaPosY() {

        int posY = 0;

        for (int x = 0; x < mapa.getLinhas(); x++) {
            for (int y = 0; y < mapa.getColunas(); y++) {
                if (mapa.getObject(x, y).equals(this)) {
                    posY = y;
                    System.out.println(posY);
                }
            }
        }
        return posY;
    }

    public String verPos() {
        return mostrarLocalizacao();
    }

    public void mover(String direcao) {

        int posAtualX, posAtualY;

        posAtualX = x;
        posAtualY = y;

        switch (direcao) {
            case "N":
                if (mapa.getObject(posAtualX - 1, posAtualY).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX - 1, posAtualY);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x--;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "S":
                if (mapa.getObject(posAtualX + 1, posAtualY).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX + 1, posAtualY);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x++;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "L":
                if (mapa.getObject(posAtualX, posAtualY + 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX, posAtualY + 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.y++;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "O":
                if (mapa.getObject(posAtualX, posAtualY - 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX, posAtualY - 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.y--;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "NE":
                if (mapa.getObject(posAtualX - 1, posAtualY + 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX - 1, posAtualY + 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x--;
                    this.y++;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "NO":
                if (mapa.getObject(posAtualX - 1, posAtualY - 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX - 1, posAtualY - 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x--;
                    this.y--;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "SE":
                if (mapa.getObject(posAtualX + 1, posAtualY + 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX + 1, posAtualY + 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x++;
                    this.y++;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
            case "SO":
                if (mapa.getObject(posAtualX + 1, posAtualY - 1).getClass().equals(Vazio.class)) {
                    mapa.setObjectAt(this, posAtualX + 1, posAtualY - 1);
                    mapa.setObjectAt(new Vazio(posAtualX, posAtualY), posAtualX, posAtualY);
                    this.x++;
                    this.y--;
                    mapa.fireTableDataChanged();
                } else {
                    System.out.println("Obstáculo detectado.");
                }
                break;
        }
    }

    /**
     * *
     * Descobre um caminho curto utilizando o algoritmo: A*
     */
    public void acharAEstrela() {
        super.paiEstrela = this;

        ArrayList<Celula> listaAberta = new ArrayList<>();
        ArrayList<Celula> listaFechada = new ArrayList<>();

        this.chegada.resetarValoresAEstrela();

        listaAberta.add(this);

        while (!listaAberta.isEmpty()) {
            //Não esquecer de adicionar o break; quando adicionarmos a chegada na lista fechada
            Celula menorF = listaAberta.get(0);
            for (int cont = 1; cont < listaAberta.size(); cont++) {
                if (menorF.getF() > listaAberta.get(cont).getF()) {
                    menorF = listaAberta.get(cont);
                }
            }
            listaAberta.remove(menorF);
            listaFechada.add(menorF);
            if (menorF.equals(chegada)) {
                System.out.println("Caminho foi achado!");
                break;
            }

            Object[] celulasOrtogonais = new Celula[4];
            Object[] celulasDiagonais = new Celula[4];

            celulasOrtogonais[0] = mapa.getObject(menorF.x, menorF.y - 1);
            celulasOrtogonais[1] = mapa.getObject(menorF.x - 1, menorF.y);
            celulasOrtogonais[2] = mapa.getObject(menorF.x, menorF.y + 1);
            celulasOrtogonais[3] = mapa.getObject(menorF.x + 1, menorF.y);

            celulasDiagonais[0] = mapa.getObject(menorF.x - 1, menorF.y + 1);
            celulasDiagonais[1] = mapa.getObject(menorF.x + 1, menorF.y - 1);
            celulasDiagonais[2] = mapa.getObject(menorF.x - 1, menorF.y - 1);
            celulasDiagonais[3] = mapa.getObject(menorF.x + 1, menorF.y + 1);

            //Interação dos ortogonais
            for (Object celulaOrtog : celulasOrtogonais) {
                if (celulaOrtog.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulaOrtog)) {
                    //Não faz nada.
                } else if (!listaAberta.contains((Celula) celulaOrtog)) {
                    listaAberta.add((Celula) celulaOrtog);
                    Celula c = (Celula) celulaOrtog;
                    c.paiEstrela = menorF;
                    c.g = paiEstrela.g + 10;
                    c.hEstrela = c.getManhattanAEstrela(chegada.x, chegada.y);
                } else {
                    int gAtual = menorF.g + 10;
                    Celula c = (Celula) celulaOrtog;
                    if (c.g > gAtual) {
                        c.paiEstrela = menorF;
                        c.g = paiEstrela.g + 10;
                    }
                }
            }

            //Interação dos diagonais
            for (Object celulaDiag : celulasDiagonais) {
                if (celulaDiag.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulaDiag)) {
                    //Não faz nada.
                } else if (!listaAberta.contains((Celula) celulaDiag)) {
                    listaAberta.add((Celula) celulaDiag);
                    Celula c = (Celula) celulaDiag;
                    c.paiEstrela = menorF;
                    c.g = paiEstrela.g + 14;
                    c.hEstrela = c.getManhattanAEstrela(chegada.x, chegada.y);
                } else {
                    int gAtual = menorF.g + 14;
                    Celula c = (Celula) celulaDiag;
                    if (c.g > gAtual) {
                        c.paiEstrela = menorF;
                        c.g = paiEstrela.g + 14;
                    }
                }
            }
        }
        //Pintar caminho

    }

    /**
     * *
     * Descobre um caminho curto utilizando o algoritmo: Melhor Primeiro
     */
    public void acharMelhorPrimeiro() {
        super.paiMelhor = this;
        ArrayList<Celula> listaAberta = new ArrayList<>();
        ArrayList<Celula> listaFechada = new ArrayList<>();

        this.chegada.resetarValoresMelhorPrimeiro();

        listaAberta.add(this);

        while (!listaAberta.isEmpty()) {
            //Não esquecer de adicionar o break; quando adicionarmos a chegada na lista fechada
            Celula menorH = listaAberta.get(0);
            for (int cont = 1; cont < listaAberta.size(); cont++) {
                if (menorH.hMelhor > listaAberta.get(cont).hMelhor) {
                    menorH = listaAberta.get(cont);
                }
            }
            listaAberta.remove(menorH);
            listaFechada.add(menorH);
            if (menorH.equals(chegada)) {
                System.out.println("Caminho foi achado!");
                break;
            }

            Object[] celulasOrtogonais = new Celula[4];
            Object[] celulasDiagonais = new Celula[4];

            celulasOrtogonais[0] = mapa.getObject(menorH.x, menorH.y - 1);
            celulasOrtogonais[1] = mapa.getObject(menorH.x - 1, menorH.y);
            celulasOrtogonais[2] = mapa.getObject(menorH.x, menorH.y + 1);
            celulasOrtogonais[3] = mapa.getObject(menorH.x + 1, menorH.y);

            celulasDiagonais[0] = mapa.getObject(menorH.x - 1, menorH.y + 1);
            celulasDiagonais[1] = mapa.getObject(menorH.x + 1, menorH.y - 1);
            celulasDiagonais[2] = mapa.getObject(menorH.x - 1, menorH.y - 1);
            celulasDiagonais[3] = mapa.getObject(menorH.x + 1, menorH.y + 1);

            //Interação dos ortogonais
            for (Object celulasOrtogonai : celulasOrtogonais) {
                if (celulasOrtogonai.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulasOrtogonai)) {
                    //Não faz nada.
                } else if (!listaAberta.contains((Celula) celulasOrtogonai)) {
                    listaAberta.add((Celula) celulasOrtogonai);
                    Celula c = (Celula) celulasOrtogonai;
                    c.paiMelhor = menorH;
                    //c.hMelhor = paiMelhor;
                    c.hMelhor = c.getManhattanMelhorPrimeiro(chegada.x, chegada.y);
                } else {
                    int hAtual = menorH.hMelhor;
                    Celula c = (Celula) celulasOrtogonai;
                    if (c.hMelhor > hAtual) {
                        c.paiMelhor = menorH;
                        c.hMelhor = paiMelhor.hMelhor;
                    }
                }
            }

            //Interação dos diagonais
            for (Object celulasDiagonai : celulasDiagonais) {
                if (celulasDiagonai.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulasDiagonai)) {
                    //Não faz nada.
                } else if (!listaAberta.contains((Celula) celulasDiagonai)) {
                    listaAberta.add((Celula) celulasDiagonai);
                    Celula c = (Celula) celulasDiagonai;
                    c.paiMelhor = menorH;
                    c.hMelhor = paiMelhor.hMelhor;
                    c.hMelhor = c.getManhattanMelhorPrimeiro(chegada.x, chegada.y);
                } else {
                    int hAtual = menorH.hMelhor;
                    Celula c = (Celula) celulasDiagonai;
                    if (c.hMelhor > hAtual) {
                        c.paiMelhor = menorH;
                        c.hMelhor = paiMelhor.hMelhor;
                    }
                }
            }
        }
        //Pintar caminho

    }

    /**
     * *
     * Descobre um caminho curto utilizando o algoritmo: Início Fim ou
     * Bidirecional
     * @return 
     */
    public Celula acharInicioFim() {
        super.paiInicioFimRobo = this;

        ArrayList<Celula> listaAberta = new ArrayList<>();
        ArrayList<Celula> listaFechada = new ArrayList<>();

        ArrayList<Celula> listaAberta2 = new ArrayList<>();
        ArrayList<Celula> listaFechada2 = new ArrayList<>();
        Celula ATUAL;
        Celula centro = null;
        
        listaAberta.add(this);

        listaAberta2.add(chegada);

        this.chegada.resetarValoresInicioFim();

        outerloop:
        while (!listaAberta.isEmpty()) {
            //Não esquecer de adicionar o break; quando adicionarmos a chegada na lista fechada
            //Começa a partir do Robô
            ATUAL = listaAberta.get(0);
            System.out.println("size1: " + listaAberta.size());
            for (int cont = 0; cont < listaAberta.size(); cont++) {

                ATUAL = listaAberta.get(cont);
                System.out.println("size: " + listaAberta2.size());
                System.out.println("mudou atual");
                listaAberta.remove(ATUAL);
                listaFechada.add(ATUAL);

                Object[] celulasOrtogonais = new Celula[4];
                Object[] celulasDiagonais = new Celula[4];

                celulasOrtogonais[0] = mapa.getObject(ATUAL.x, ATUAL.y - 1);
                celulasOrtogonais[1] = mapa.getObject(ATUAL.x - 1, ATUAL.y);
                celulasOrtogonais[2] = mapa.getObject(ATUAL.x, ATUAL.y + 1);
                celulasOrtogonais[3] = mapa.getObject(ATUAL.x + 1, ATUAL.y);

                celulasDiagonais[0] = mapa.getObject(ATUAL.x - 1, ATUAL.y + 1);
                celulasDiagonais[1] = mapa.getObject(ATUAL.x + 1, ATUAL.y - 1);
                celulasDiagonais[2] = mapa.getObject(ATUAL.x - 1, ATUAL.y - 1);
                celulasDiagonais[3] = mapa.getObject(ATUAL.x + 1, ATUAL.y + 1);

                //Interação dos ortogonais
                for (Object celulasOrtogonai : celulasOrtogonais) {
                    if (celulasOrtogonai.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulasOrtogonai)) {
                        //Não faz nada.
                    } else if (!listaAberta.contains((Celula) celulasOrtogonai)) {
                        listaAberta.add((Celula) celulasOrtogonai);
                        Celula c = (Celula) celulasOrtogonai;
                        c.paiInicioFimRobo = ATUAL;
                    }
                }

                //Interação dos diagonais
                for (Object celulasDiagonai : celulasDiagonais) {
                    if (celulasDiagonai.getClass() == Obstaculo.class || listaFechada.contains((Celula) celulasDiagonai)) {
                        //Não faz nada.
                    } else if (!listaAberta.contains((Celula) celulasDiagonai)) {
                        listaAberta.add((Celula) celulasDiagonai);
                        Celula c = (Celula) celulasDiagonai;
                        c.paiInicioFimRobo = ATUAL;
                    }
                }
            }

            //ao contrario
            //Começa a partir do destino
            Celula ATUAL2 = listaAberta2.get(0);
            System.out.println("size2: " + listaAberta2.size());
            for (int cont = 0; cont < listaAberta2.size(); cont++) {
                ATUAL2 = listaAberta2.get(cont);
                System.out.println("size2: " + listaAberta2.size());
                System.out.println("mudou atual2");
                listaAberta2.remove(ATUAL2);
                listaFechada2.add(ATUAL2);

                Object[] celulasOrtogonais2 = new Celula[4];
                Object[] celulasDiagonais2 = new Celula[4];

                celulasOrtogonais2[0] = mapa.getObject(ATUAL2.x, ATUAL2.y - 1);
                celulasOrtogonais2[1] = mapa.getObject(ATUAL2.x - 1, ATUAL2.y);
                celulasOrtogonais2[2] = mapa.getObject(ATUAL2.x, ATUAL2.y + 1);
                celulasOrtogonais2[3] = mapa.getObject(ATUAL2.x + 1, ATUAL2.y);

                celulasDiagonais2[0] = mapa.getObject(ATUAL2.x - 1, ATUAL2.y + 1);
                celulasDiagonais2[1] = mapa.getObject(ATUAL2.x + 1, ATUAL2.y - 1);
                celulasDiagonais2[2] = mapa.getObject(ATUAL2.x - 1, ATUAL2.y - 1);
                celulasDiagonais2[3] = mapa.getObject(ATUAL2.x + 1, ATUAL2.y + 1);

                //Interação dos ortogonais
                for (Object celulasOrtogonai2 : celulasOrtogonais2) {
                    if (celulasOrtogonai2.getClass() == Obstaculo.class || listaFechada2.contains((Celula) celulasOrtogonai2)) {
                        //Não faz nada.
                    } else if (!listaAberta2.contains((Celula) celulasOrtogonai2)) {
                        listaAberta2.add((Celula) celulasOrtogonai2);
                        Celula c2 = (Celula) celulasOrtogonai2;
                        c2.paiInicioFimDestino = ATUAL2;
                    }
                }

                //Interação dos diagonais
                for (Object celulasDiagonai2 : celulasDiagonais2) {
                    if (celulasDiagonai2.getClass() == Obstaculo.class || listaFechada2.contains((Celula) celulasDiagonai2)) {
                        //Não faz nada.
                    } else if (!listaAberta2.contains((Celula) celulasDiagonai2)) {
                        listaAberta2.add((Celula) celulasDiagonai2);
                        Celula c2 = (Celula) celulasDiagonai2;
                        c2.paiInicioFimDestino = ATUAL2;
                    }
                }

                if (listaAberta2.contains(ATUAL)) {
                    /*
                    Entra aqui se alguma célula das listas convergiram
                    em alguma célula
                    */
                    System.out.println("We found each other at: " + ATUAL);
                    centro = ATUAL;
                    break outerloop;
                }
            }
        }
        System.out.println("Busca Inicio-Fim finalizada.");
        return centro;
    }
}
