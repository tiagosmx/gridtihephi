/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package janelas;

import modelo.Celula;
import modelo.ModeloTabelaMapa;
import modelo.Robo;

/**
 *
 * @author Tito
 */
public class JanelaMapa extends javax.swing.JFrame {

    /**
     * Creates new form JanelaMapa
     */
    ModeloTabelaMapa mapa;
    Robo robo;
    int linhas;
    int colunas;

    public JanelaMapa(int numLinhas, int numColunas) {
        this.linhas = numLinhas;
        this.colunas = numColunas;
        this.mapa = new ModeloTabelaMapa(linhas, colunas);

        initComponents();
        //Este setCelRenderer é chamado aqui pois ele só funciona depois que
        //a tabela foi criada, diferente dos modelos, que precisam ser iniciados
        //antes
        for (int i = 0; i < colunas; i++) {
            this.jtMapa.getColumnModel().getColumn(i).setCellRenderer(new FormatacaoMapa());
        }
        this.jVariaveis.setLocationRelativeTo(this);
        this.jVariaveis.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jVariaveis = new javax.swing.JDialog();
        lbX = new javax.swing.JLabel();
        lbY = new javax.swing.JLabel();
        lbH = new javax.swing.JLabel();
        lbG = new javax.swing.JLabel();
        lbQ1 = new javax.swing.JLabel();
        lbAEstrela = new javax.swing.JLabel();
        lbPaiY = new javax.swing.JLabel();
        lbPaiX = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtMapa = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        botaoCentral = new javax.swing.JButton();
        botaoNE = new javax.swing.JButton();
        botaoO = new javax.swing.JButton();
        botaoN = new javax.swing.JButton();
        boatoL = new javax.swing.JButton();
        botaoSE = new javax.swing.JButton();
        botaoNO = new javax.swing.JButton();
        botaoS = new javax.swing.JButton();
        botaoSO = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btMelhorPrimeiro = new javax.swing.JButton();
        btInicioFim = new javax.swing.JButton();
        btAEstrela = new javax.swing.JButton();
        btResetar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lbLegendaF = new javax.swing.JLabel();
        lbLegendaR = new javax.swing.JLabel();
        lbLegendaX = new javax.swing.JLabel();

        jVariaveis.setTitle("Variáveis");
        jVariaveis.setMinimumSize(new java.awt.Dimension(200, 200));

        lbX.setBorder(javax.swing.BorderFactory.createTitledBorder("X"));

        lbY.setBorder(javax.swing.BorderFactory.createTitledBorder("Y"));

        lbH.setBorder(javax.swing.BorderFactory.createTitledBorder("H"));

        lbG.setBorder(javax.swing.BorderFactory.createTitledBorder("G"));

        lbQ1.setBorder(javax.swing.BorderFactory.createTitledBorder("?"));

        lbAEstrela.setBorder(javax.swing.BorderFactory.createTitledBorder("A*"));

        lbPaiY.setBorder(javax.swing.BorderFactory.createTitledBorder("PaiY"));

        lbPaiX.setBorder(javax.swing.BorderFactory.createTitledBorder("PaiX"));

        javax.swing.GroupLayout jVariaveisLayout = new javax.swing.GroupLayout(jVariaveis.getContentPane());
        jVariaveis.getContentPane().setLayout(jVariaveisLayout);
        jVariaveisLayout.setHorizontalGroup(
            jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jVariaveisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jVariaveisLayout.createSequentialGroup()
                        .addComponent(lbX, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbY, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                    .addGroup(jVariaveisLayout.createSequentialGroup()
                        .addComponent(lbG, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbH, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                    .addGroup(jVariaveisLayout.createSequentialGroup()
                        .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbPaiX, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addComponent(lbQ1, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbAEstrela, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                            .addComponent(lbPaiY, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jVariaveisLayout.setVerticalGroup(
            jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jVariaveisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbX, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                    .addComponent(lbY, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbH, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                    .addComponent(lbG, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbQ1, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                    .addComponent(lbAEstrela, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jVariaveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbPaiX, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                    .addComponent(lbPaiY, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Grid Tihephi");

        jtMapa.setModel(this.mapa);
        jtMapa.setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
        jtMapa.getTableHeader().setResizingAllowed(false);
        jtMapa.getTableHeader().setReorderingAllowed(false);
        jtMapa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtMapaMouseReleased(evt);
            }
        });
        jtMapa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtMapaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jtMapa);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Mover Robô"));

        botaoCentral.setFont(botaoCentral.getFont().deriveFont(botaoCentral.getFont().getSize()+26f));
        botaoCentral.setText("⊕");
        botaoCentral.setAlignmentY(0.0F);
        botaoCentral.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botaoCentral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCentralActionPerformed(evt);
            }
        });

        botaoNE.setFont(botaoNE.getFont().deriveFont(botaoNE.getFont().getSize()+15f));
        botaoNE.setText("↗");
        botaoNE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNEActionPerformed(evt);
            }
        });

        botaoO.setFont(botaoO.getFont().deriveFont(botaoO.getFont().getSize()+15f));
        botaoO.setText("←");
        botaoO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoOActionPerformed(evt);
            }
        });

        botaoN.setFont(botaoN.getFont().deriveFont(botaoN.getFont().getSize()+15f));
        botaoN.setText("↑");
        botaoN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNActionPerformed(evt);
            }
        });

        boatoL.setFont(boatoL.getFont().deriveFont(boatoL.getFont().getSize()+15f));
        boatoL.setText("→");
        boatoL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boatoLActionPerformed(evt);
            }
        });

        botaoSE.setFont(botaoSE.getFont().deriveFont(botaoSE.getFont().getSize()+15f));
        botaoSE.setText("↘");
        botaoSE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSEActionPerformed(evt);
            }
        });

        botaoNO.setFont(botaoNO.getFont().deriveFont(botaoNO.getFont().getSize()+15f));
        botaoNO.setText("↖");
        botaoNO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoNOActionPerformed(evt);
            }
        });

        botaoS.setFont(botaoS.getFont().deriveFont(botaoS.getFont().getSize()+15f));
        botaoS.setText("↓");
        botaoS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSActionPerformed(evt);
            }
        });

        botaoSO.setFont(botaoSO.getFont().deriveFont(botaoSO.getFont().getSize()+15f));
        botaoSO.setText("↙");
        botaoSO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSOActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(botaoNO)
                    .addComponent(botaoSO)
                    .addComponent(botaoO))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botaoCentral)
                    .addComponent(botaoS)
                    .addComponent(botaoN))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botaoNE)
                    .addComponent(botaoSE)
                    .addComponent(boatoL))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {boatoL, botaoCentral, botaoN, botaoNE, botaoNO, botaoO, botaoS, botaoSE, botaoSO});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(botaoNE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(boatoL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(6, 6, 6)
                            .addComponent(botaoSE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(botaoN)
                            .addGap(6, 6, 6)
                            .addComponent(botaoCentral)
                            .addGap(6, 6, 6)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(botaoS)
                                .addComponent(botaoSO))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(botaoNO)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoO)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {boatoL, botaoCentral, botaoN, botaoNE, botaoNO, botaoO, botaoS, botaoSE, botaoSO});

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscas"));

        btMelhorPrimeiro.setFont(btMelhorPrimeiro.getFont().deriveFont(btMelhorPrimeiro.getFont().getSize()+4f));
        btMelhorPrimeiro.setText("Caminho Melhor-Primeiro");
        btMelhorPrimeiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMelhorPrimeiroActionPerformed(evt);
            }
        });

        btInicioFim.setFont(btInicioFim.getFont().deriveFont(btInicioFim.getFont().getSize()+4f));
        btInicioFim.setText("Caminho Inicio-Fim");
        btInicioFim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInicioFimActionPerformed(evt);
            }
        });

        btAEstrela.setFont(btAEstrela.getFont().deriveFont(btAEstrela.getFont().getSize()+4f));
        btAEstrela.setText("Caminho A*");
        btAEstrela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAEstrelaActionPerformed(evt);
            }
        });

        btResetar.setFont(btResetar.getFont().deriveFont(btResetar.getFont().getSize()+4f));
        btResetar.setText("Resetar Mapa");
        btResetar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btResetarActionPerformed(evt);
            }
        });

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getSize()+2f));
        jLabel1.setText("Antes de executar a busca, clique na célula do robô e clique no símbolo ⊕");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btAEstrela, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btInicioFim, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btMelhorPrimeiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btResetar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btMelhorPrimeiro, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btInicioFim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btAEstrela)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btResetar))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btAEstrela, btInicioFim, btMelhorPrimeiro, btResetar});

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Legendas"));

        lbLegendaF.setBackground(java.awt.Color.red);
        lbLegendaF.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        lbLegendaF.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbLegendaF.setText("F = Fim");
        lbLegendaF.setOpaque(true);

        lbLegendaR.setBackground(java.awt.Color.green);
        lbLegendaR.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        lbLegendaR.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbLegendaR.setText("R = Robô");
        lbLegendaR.setOpaque(true);

        lbLegendaX.setBackground(java.awt.Color.lightGray);
        lbLegendaX.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        lbLegendaX.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbLegendaX.setText("X = Obstáculo");
        lbLegendaX.setOpaque(true);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbLegendaX, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbLegendaR, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLegendaF, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(lbLegendaX, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbLegendaF, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbLegendaR, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botaoNOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNOActionPerformed
        if (robo != null) {
            robo.mover("NO");
        }
    }//GEN-LAST:event_botaoNOActionPerformed

    private void botaoNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNActionPerformed
        if (robo != null) {
            robo.mover("N");
        }
    }//GEN-LAST:event_botaoNActionPerformed

    private void botaoOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoOActionPerformed
        if (robo != null) {
            robo.mover("O");
        }
    }//GEN-LAST:event_botaoOActionPerformed

    private void botaoSOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSOActionPerformed
        if (robo != null) {
            robo.mover("SO");
        }
    }//GEN-LAST:event_botaoSOActionPerformed

    private void botaoSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSActionPerformed
        if (robo != null) {
            robo.mover("S");
        }
    }//GEN-LAST:event_botaoSActionPerformed

    private void botaoSEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSEActionPerformed
        if (robo != null) {
            robo.mover("SE");
        }
    }//GEN-LAST:event_botaoSEActionPerformed

    private void boatoLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boatoLActionPerformed
        if (robo != null) {
            robo.mover("L");
        }
    }//GEN-LAST:event_boatoLActionPerformed

    private void botaoNEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoNEActionPerformed
        if (robo != null) {
            robo.mover("NE");
        }
    }//GEN-LAST:event_botaoNEActionPerformed

    private void botaoCentralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCentralActionPerformed
        // TODO add your handling code here:
        int y = jtMapa.getSelectedColumn();
        int x = jtMapa.getSelectedRow();
        if (x < 0 || y < 0) {
            System.out.println("Não consegui entender, selecione novamente a célula desejada.");
        } else {
            robo = mapa.getRobotAt(x, y);
            if (robo == null) {
                System.out.println("Não detectei nenhum robô onde você indicou.");
            }
        }
    }//GEN-LAST:event_botaoCentralActionPerformed

    private void btAEstrelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAEstrelaActionPerformed
        if (robo != null) {
            //Inicia a busca por um caminho ótimamente curto através do A*
            robo.acharAEstrela();
            //Cria uma variável para iterar, inicializando ela com a chegada
            Celula caminhoCelula = (Celula) robo.chegada;
            if (caminhoCelula.paiEstrela != null) {
                //Percorre todo o caminho até chegar ao robô
                while (!caminhoCelula.equals((Celula) robo)) {
                    caminhoCelula.isCaminhoAEstrela = true;
                    caminhoCelula = caminhoCelula.paiEstrela;
                }
                this.mapa.fireTableDataChanged();
            } else {
                System.out.println("Caminho não encontrado.");
            }
        } else {
            System.out.println("Robô não foi selecionado. Selecione o robô desejado.");
        }
    }//GEN-LAST:event_btAEstrelaActionPerformed

    private void jtMapaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtMapaMouseReleased
        atualizarLabelsTexto();
    }//GEN-LAST:event_jtMapaMouseReleased

    private void jtMapaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtMapaKeyReleased
        if (evt.isActionKey()) {
            atualizarLabelsTexto();
        }
    }//GEN-LAST:event_jtMapaKeyReleased

    private void btMelhorPrimeiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMelhorPrimeiroActionPerformed
        if (robo != null) {
            //Inicia a busca por um caminho ótimamente curto através do A*
            robo.acharMelhorPrimeiro();
            //Cria uma variável para iterar, inicializando ela com a chegada
            Celula caminhoCelula = (Celula) robo.chegada;
            if (caminhoCelula.paiMelhor != null) {
                //Percorre todo o caminho até chegar ao robô
                while (!caminhoCelula.equals((Celula) robo)) {
                    caminhoCelula.isCaminhoMelhorPrimeiro = true;
                    caminhoCelula = caminhoCelula.paiMelhor;
                }
                this.mapa.fireTableDataChanged();
            } else {
                System.out.println("Caminho não encontrado.");
            }
        } else {
            System.out.println("Robô não foi selecionado. Selecione o robô desejado.");
        }
    }//GEN-LAST:event_btMelhorPrimeiroActionPerformed

    /**
     * *
     * Limpa a matriz, resetando os valores adicionados pelos algoritmos no item
     * Célula
     *
     * @param evt
     */
    private void btResetarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btResetarActionPerformed
        this.mapa.limparDadosDasBuscas();
    }//GEN-LAST:event_btResetarActionPerformed

    private void btInicioFimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInicioFimActionPerformed
        if (robo != null) {
            Celula centro = robo.acharInicioFim();
            if (centro != null) {
                centro.isCaminhoInicioFim = true;
                Celula caminhoRobo = centro.paiInicioFimRobo;
                Celula caminhoChegada = centro.paiInicioFimDestino;
                while (!caminhoRobo.equals((Celula) robo)) {
                    caminhoRobo.isCaminhoInicioFim = true;
                    caminhoRobo = caminhoRobo.paiInicioFimRobo;
                }
                while (!caminhoChegada.equals((Celula) robo.chegada)) {
                    caminhoChegada.isCaminhoInicioFim = true;
                    caminhoChegada = caminhoChegada.paiInicioFimDestino;
                }

                this.mapa.fireTableDataChanged();
            } else {
                System.out.println("Caminho não encontrado.");
            }
        } else {
            System.out.println("Robô não foi selecionado. Selecione o robô desejado.");
        }

    }//GEN-LAST:event_btInicioFimActionPerformed

    private void atualizarLabelsTexto() {
        //Atualiza os valores da janela flutuante que descreve o conteúdo
        //das variaveis de cada célula
        int linha = this.jtMapa.getSelectedRow();
        int coluna = this.jtMapa.getSelectedColumn();

        if (linha < 0 || coluna < 0) {
            //Linha e coluna inexistente
        } else {
            Celula objSelecionado = mapa.getCelula(linha, coluna);
            lbAEstrela.setText("" + objSelecionado.isCaminhoAEstrela);
            lbG.setText("" + objSelecionado.g);
            lbH.setText("" + objSelecionado.hEstrela);
            lbX.setText("" + objSelecionado.x);
            lbY.setText("" + objSelecionado.y);
            if (objSelecionado.paiEstrela != null) {
                lbPaiX.setText("" + objSelecionado.paiEstrela.x);
                lbPaiY.setText("" + objSelecionado.paiEstrela.y);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JanelaMapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JanelaMapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JanelaMapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JanelaMapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                //new JanelaMapa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boatoL;
    private javax.swing.JButton botaoCentral;
    private javax.swing.JButton botaoN;
    private javax.swing.JButton botaoNE;
    private javax.swing.JButton botaoNO;
    private javax.swing.JButton botaoO;
    private javax.swing.JButton botaoS;
    private javax.swing.JButton botaoSE;
    private javax.swing.JButton botaoSO;
    private javax.swing.JButton btAEstrela;
    private javax.swing.JButton btInicioFim;
    private javax.swing.JButton btMelhorPrimeiro;
    private javax.swing.JButton btResetar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JDialog jVariaveis;
    private javax.swing.JTable jtMapa;
    public javax.swing.JLabel lbAEstrela;
    public javax.swing.JLabel lbG;
    public javax.swing.JLabel lbH;
    private javax.swing.JLabel lbLegendaF;
    private javax.swing.JLabel lbLegendaR;
    private javax.swing.JLabel lbLegendaX;
    public javax.swing.JLabel lbPaiX;
    public javax.swing.JLabel lbPaiY;
    public javax.swing.JLabel lbQ1;
    public javax.swing.JLabel lbX;
    public javax.swing.JLabel lbY;
    // End of variables declaration//GEN-END:variables
}
