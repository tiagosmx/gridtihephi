/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package janelas;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import modelo.ModeloTabelaMapa;

/**
 *
 * @author Tito
 */
public class FormatacaoMapa extends DefaultTableCellRenderer {

    public FormatacaoMapa() {
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        //Cells are by default rendered as a JLabel.
        JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
        l.setHorizontalAlignment(JLabel.CENTER);

        //Get the status for the current row.
        ModeloTabelaMapa tableModel = (ModeloTabelaMapa) table.getModel();
        if (null != tableModel.getValueAt(row, col)) {
            switch (tableModel.getValueAt(row, col)) {
                case "":
                    l.setBackground(Color.WHITE);
                    break;
                case "X":
                    l.setBackground(Color.GRAY);
                    break;
                case "R":
                    l.setBackground(Color.GREEN);
                    break;
                case "F":
                    l.setBackground(Color.RED);
                    break;
                default:
                    l.setBackground(Color.WHITE);
                    break;
            }
        }

        //Caso especial: A*
        if (tableModel.getCelula(row, col).isCaminhoAEstrela && !tableModel.getValueAt(row, col).equals("F")) {
            l.setBackground(Color.YELLOW);
        }
        if (tableModel.getCelula(row, col).isCaminhoMelhorPrimeiro && !tableModel.getValueAt(row, col).equals("F")) {
            l.setBackground(Color.PINK);
        }
        if (tableModel.getCelula(row, col).isCaminhoMelhorPrimeiro
                && !tableModel.getValueAt(row, col).equals("F")
                && tableModel.getCelula(row, col).isCaminhoAEstrela) {
            //#FF738C
            l.setBackground(Color.ORANGE);
        }
        if (tableModel.getCelula(row, col).isCaminhoInicioFim && !tableModel.getValueAt(row, col).equals("F")) {
            l.setBackground(Color.BLACK);
        }

        //Return the JLabel which renders the cell.
        return l;

    }
}
